import { Component, OnInit } from '@angular/core';

import { KeycloakService } from 'keycloak-angular';

import { Role } from '@shared/utils/roles';

import { environment } from '@env'

/**
  * Home principale qui fera affiché le home correspondant au rôle de l'utilisateur connecté.
  * Les homes disponibles sont : home-assistante, home-collaborateur, home-commercial et home-rh.
  * Le rôle dépend directement du rôle Keycloak et est donc récupérér dans l'instance de Keycloak.
  */
@Component({
  selector: 'app-home',
  templateUrl: './home.component.html'
})
export class HomeComponent implements OnInit {

  role = Role;
  /**
    * Le rôle de l'utilisateur.
    */
  userRole : string;
  constructor(private keycloakService : KeycloakService) {
    let clientId = environment.keycloakConfig.clientId;
    this.userRole = this.keycloakService.getKeycloakInstance().resourceAccess[clientId]["roles"][0];
  }

  ngOnInit() {
  }
}
