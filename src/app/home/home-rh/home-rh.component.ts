import { Component, OnInit, ViewChild } from '@angular/core';

import { Observable, Subscription } from 'rxjs';

import {MatTableDataSource} from '@angular/material/table';
import {MatPaginator} from '@angular/material/paginator';
import {MatSort} from '@angular/material/sort';

import { FormationModel } from "@shared/api-swagger/model/models";
import { DisplayFormation } from "@shared/displayInfo/displays";
import { FormationsService } from "@shared/api-swagger/api/api";


/**
  * Le composant home qui sera affiché pour les RH.
  * Cette interface affiche la liste des prochaines formations.
  * Les informations qui seront affichées sont : le libellé, la date prévu, le nombre de participant, l'état de la formation.
  * Il est possible d'accéder aux informations de la formation depuis le home.
  */
@Component({
  selector : 'home-rh',
  templateUrl : 'home-rh.component.html'
})
export class HomeRHComponent implements OnInit {


  formationsDisponibles : DisplayFormation[];
  formationsFiltres : DisplayFormation[];
  private formationsDisponiblesSubscription : Subscription;

  //displayedColumns: string[] = ["intitule", "participants", "date", "origine", "statut"]
  displayedColumns: string[] = ["intitule", "origine",  "participants", "date", "statut"]

  /**
    * source pour l'affichage des formations dans le tableau qui est affichée.
    */
  dataSource : MatTableDataSource<DisplayFormation>;

  /**
    * contenu de la recherche.
    */
  search = "";

  /**
    * Pagination du tableau.
    */
  @ViewChild(MatPaginator) paginator: MatPaginator;

  /**
    * Tri par les éléments du tableau selon la colonne choisie.
    */
  @ViewChild(MatSort) sort: MatSort;

  /**
    * Spécifie si la liste des EP est en cours de chargement et d'écriture dans le tableau.
    */
  chargement = true;
  constructor(private service:FormationsService) {
  }

  ngOnInit() {
    this.formationsDisponiblesSubscription = this.service.getProchainesFormation(undefined, 1).subscribe(
      formations => this.initFormations(formations)
    );
  }

  initFormations(formations:FormationModel[]) {
    this.formationsDisponibles = [];
    let formationDisplay : DisplayFormation;
    for(let formation of formations) {
      formationDisplay = new DisplayFormation();
      formationDisplay.id = formation.id;
      formationDisplay.intitule = formation.intitule;
      formationDisplay.nbParticipants = 0;
      if(formation.participantsFormation != undefined) {
        formationDisplay.nbParticipants = formation.participantsFormation.length;
      }
      formationDisplay.datePrevu = formation.dateDebut;
      formationDisplay.origine = formation.origine;
      formationDisplay.statut = formation.statut;
      this.formationsDisponibles.push(formationDisplay);
    }
    this.formationsFiltres = this.formationsDisponibles;
    this.dataSource = new MatTableDataSource(this.formationsFiltres);
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
  }

  ngOnDestroy() {
    if(this.formationsDisponiblesSubscription != undefined) {
      this.formationsDisponiblesSubscription.unsubscribe();
    }
  }
}
