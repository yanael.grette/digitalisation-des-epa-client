import { Component, OnInit } from '@angular/core';


/**
  * Le composant home qui sera affiché uniquement pour les collaborateurs.
  * Cette interface permet au collaborateur de savoir le nombre de temps restant avant la possibilité de saisir son EP.
  * Une fois l'EP saisie, il aura les informations nécessaires sur son EP (la date, l'état de l'EP et son référent pour l'EP).
  * Dans le cas des collaborateurs référents, ils auront aussi la liste des prochains EP qu'ils devront faire passer.
  */
@Component({
  selector : 'home-collaborateur',
  templateUrl : 'home-collaborateur.component.html'
})
export class HomeCollaborateurComponent implements OnInit {
  constructor() {
  }

  ngOnInit() {
  }
}
