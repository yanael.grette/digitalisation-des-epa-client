import { Component, OnInit, OnDestroy, ViewChild, ViewChildren, AfterViewInit } from '@angular/core';

import { KeycloakService } from 'keycloak-angular';
import { Observable, Subscription } from 'rxjs';

import {MatTableDataSource} from '@angular/material/table';
import {MatPaginator} from '@angular/material/paginator';
import {MatSort} from '@angular/material/sort';

import { CollaborateurModel } from "@shared/api-swagger/model/collaborateurModel";
import { EpModel } from "@shared/api-swagger/model/epModel";
import { DisplayEP } from "@shared/displayInfo/displays";
import { EpService } from "@shared/api-swagger/api/api";


/**
  * Le composant home qui sera affiché uniquement pour les assistantes.
  * Cette interface affiche la liste des EP disponibles et en cours, de les trier et de faire une recherche avec la barre de recherche mais aussi choisir un intervalle de dates.
  * Le tri peut se faire par rapport aux éléments du tableau et de checkboxes des agences et de si les EP ont été terminé et non signé.
  * Les assistantes peuvent accéder directement aux informations du collaborateur concerné, du référent actuel et de l'EP, si ce dernier a été saisie.
  * LEs informations qui seront affichées sont : l'agence, le collaborateur, son ancienneté, le référent, la date d'envoie du mail, l'état, le type d'EP et la date prévu d'entretien.
  */
@Component({
  selector : 'home-assistante',
  templateUrl : 'home-assistante.component.html',
  styleUrls : [ "./home-assistante.component.css"]
})
export class HomeAssistanteComponent implements OnInit, AfterViewInit {

  /**
    * Une liste qui contiendra la liste des EP avec les informations à faire conserver.
    */
  epDisponibles : DisplayEP[];

  /**
    * Liste des EP qui seront affichées en fonction des filtres.
    */
  epFiltre : DisplayEP[];

  /**
    * Subscription pour récupérer les EP.
    */
  private epDisponiblesSubscription : Subscription;

  /**
    * liste des titres des colonnes du tableau.
    */
  //displayedColumns: string[] = ["agence", "info", "datemail", "dateentretien", "etat", "type"];
  displayedColumns: string[] = ["agence", "collaborateur", "anciennete", "referent", "type", "etat", "datemail", "dateentretien"  ];

  /**
    * source pour l'affichage des EP dans le tableau qui est affichée.
    */
  dataSource : MatTableDataSource<DisplayEP>;

  /**
    * contenu de la recherche.
    */
  search = "";

  /**
    * Pagination du tableau.
    */
  @ViewChild(MatPaginator) paginator: MatPaginator;

  /**
    * Tri par les éléments du tableau selon la colonne choisie.
    */
  @ViewChild(MatSort) sort: MatSort;

  /**
    * Spécifie si la liste des EP est en cours de chargement et d'écriture dans le tableau.
    */
  chargement = true;

  constructor(public keycloakService : KeycloakService, private service:EpService) {
  }

  /**
    * Récupérer la liste des EP Disponibles dès l'initialisation.
    */
  ngOnInit() {
    //this.epDisponiblesSubscription = this.serviceEP.listeEPDisponibles().
    //this.epDisponiblesSubscription = this.service.collaborateursBuIdBuGet("ORL").
    let idAgence = 1;
    this.epDisponiblesSubscription = this.service.getProchainsEP(undefined, 1).
       subscribe(eps => {
         this.initDisplay(eps);
         this.refreshDataSource();
       });
  }

  ngAfterViewInit(){}

  /**
    * Initialisation de l'affichage des EP dans le tableau.
    */
  initDisplay(eps: EpModel[]) {
    this.epDisponibles = []
    let epDisplay : DisplayEP;
    let today = new Date();
    console.log(eps);
    for(let ep of eps) {
      epDisplay = new DisplayEP();
      epDisplay.id = ep.id;
      epDisplay.agence = ep.collaborateur.businessUnit.nom;
      epDisplay.collaborateur = ep.collaborateur;
      epDisplay.referent = ep.referent;
      epDisplay.dateentretien = ep.dateEntretien;
      epDisplay.datemail = ep.dateDisponibilite;
      epDisplay.etat = ep.etat;
      epDisplay.type = ep.type;
      epDisplay.anciennete = this.setAnciennete(new Date(ep.collaborateur.dateArrivee), today);
      epDisplay.annee = Math.floor(epDisplay.anciennete / 31536000000);
      epDisplay.mois = Math.floor(epDisplay.anciennete / 2629800000 % 12);
      this.epDisponibles.push(epDisplay);
    }
    this.epFiltre = this.epDisponibles;
  }

  getAnciennete(anciennete) {
    let annee = Math.floor(anciennete / 31536000000);
    let mois = Math.floor(anciennete/ 2629800000 % 12);
    return annee + " an(s) et "+ mois+" mois";
  }

  setAnciennete(firstDate, secondDate) {
    return Math.abs(firstDate-secondDate);
  }

  getEtat(etat) {
    let res = "";
    switch(etat) {
      case 1:
        res= "Disponible";
        break;
      case 2:
        res= "Saisi";
        break;
      case 3:
        res= "Dates proposées";
        break;
      case 4:
        res= "Entretien prévu";
        break;
      case 5:
        res= "Attente Signature";
        break;
    }
    return res;
  }

  /**
    * Mise à jour du tableau lorsque qu'un tri est fait via les checkboxes.
    */
  refreshDataSource() {
    this.dataSource = new MatTableDataSource(this.epFiltre);
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
  }




  /**
    * Cette fonction permet de mettre à jour les informations du tableau en fontionne de la barre de recherche
    */
  applyFilter(filterValue: string) {
    console.log(filterValue);
    filterValue = filterValue.trim();
    filterValue = filterValue.toLowerCase();
    this.dataSource.filter = filterValue;
  }

  /**
    * Détruire toutes les Subscriptions utilisées pour libérer de la mémoire après le changement de page.
    */
  ngOnDestroy() {
    if(this.epDisponiblesSubscription != undefined) {
      this.epDisponiblesSubscription.unsubscribe();
    }
  }
}
