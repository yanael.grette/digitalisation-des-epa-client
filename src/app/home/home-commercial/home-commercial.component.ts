import { Component, OnInit } from '@angular/core';


/**
  * Le composant home qui sera affiché uniquement pour les commerciaux.
  * Cette interface affiche la liste des prochains EP que feront passer les commerciaux.
  * Les informations qui seront affichées sont : l'agence, le collaborateur, l'ancienneté du collaborateur, l'état de l'EP, la date d'entretien prévu.
  * Les commerciaux peuvent trier le tableau d'EP par rapport aux colonnes, par agences et utiliser une barre de recherche.
  */
@Component({
  selector : 'home-commercial',
  templateUrl : 'home-commercial.component.html'
})
export class HomeCommercialComponent implements OnInit {
  constructor() {
  }

  ngOnInit() {
  }
}
