import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { RouterModule } from '@angular/router';
import { FormsModule } from '@angular/forms';

import { MaterialModule } from "@shared/angular-material/angular-material.module";

import {HomeAssistanteComponent} from './home-assistante/home-assistante.component';
import {HomeCollaborateurComponent} from './home-collaborateur/home-collaborateur.component';
import {HomeCommercialComponent} from './home-commercial/home-commercial.component';
import {HomeRHComponent} from './home-rh/home-rh.component';
import {HomeComponent} from './home.component';
import {NavMenuModule} from '@shared/nav-menu/nav-menu.module';

@NgModule({
  declarations: [
    HomeComponent, HomeAssistanteComponent, HomeCollaborateurComponent,
    HomeCommercialComponent, HomeRHComponent
  ],
  exports: [
    HomeComponent
  ],
  imports: [
    CommonModule,
    FormsModule,
    MaterialModule,
    NavMenuModule,
    RouterModule
  ],
})
export class HomeModule {}
