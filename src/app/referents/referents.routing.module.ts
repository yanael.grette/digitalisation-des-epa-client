import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';


import { ReferentsComponent } from "./referents.component";
import { DetailsReferentComponent } from "./details-referent/details-referent.component";

import { paths_referents } from "@shared/utils/paths";

import { KeycloakGuard } from '@shared/guards/keycloakguard';

const routes: Routes = [
  {
    path:'',
    component: ReferentsComponent,
    pathMatch: 'full',
    canActivate: [KeycloakGuard]
  },
  {
    path:paths_referents.get,
    component: DetailsReferentComponent,
    canActivate: [KeycloakGuard]
  }
];

/*
/referents
/referents/:id (id référent)

*/

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ReferentsRoutingModule {}
