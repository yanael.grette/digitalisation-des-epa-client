import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { RouterModule } from '@angular/router';

import { MaterialModule } from "@shared/angular-material/angular-material.module";

import {NavMenuModule} from '@shared/nav-menu/nav-menu.module';

import { ReferentsComponent } from './referents.component';
import { DetailsReferentComponent } from './details-referent/details-referent.component';
import { ReferentsRoutingModule } from './referents.routing.module';

@NgModule({
  declarations: [ DetailsReferentComponent, ReferentsComponent
  ],
  exports: [ ReferentsComponent
  ],
  imports: [
    CommonModule,
    MaterialModule,
    NavMenuModule,
    RouterModule,
    ReferentsRoutingModule
  ],
})
export class ReferentsModule {}
