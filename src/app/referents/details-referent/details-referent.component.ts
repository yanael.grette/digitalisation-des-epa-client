import { Component, OnInit, ViewChild, OnDestroy } from '@angular/core';

import { Subscription } from 'rxjs';
import {ActivatedRoute} from '@angular/router';

import {MatTableDataSource} from '@angular/material/table';
import {MatPaginator} from '@angular/material/paginator';
import {MatSort} from '@angular/material/sort';

import { ReferentsService } from "@shared/api-swagger/api/api";

import { CollaborateurModel } from "@shared/api-swagger/model/collaborateurModel";
import { EpModel } from "@shared/api-swagger/model/epModel";
import { DisplayEP } from "@shared/displayInfo/displays";

/**
    */
@Component({
  selector: 'app-details-referent',
  templateUrl: './details-referent.component.html'
})
export class DetailsReferentComponent implements OnInit {
  referent: CollaborateurModel;
  private referentSubscription : Subscription;
  idReferent : any;


  constructor(private service:ReferentsService, private route: ActivatedRoute) {}

  ngOnInit() {
    this.idReferent = this.route.snapshot.paramMap.get("id");
    this.referentSubscription = this.service.getReferentById(this.idReferent).subscribe(
      referent => this.initReferent(referent)
    )
  }

  initReferent(referent:CollaborateurModel) {
    this.referent = referent;
  }

  ngOnDestroy() {
    if(this.referentSubscription!=undefined) {
      this.referentSubscription.unsubscribe();
    }
  }
}
