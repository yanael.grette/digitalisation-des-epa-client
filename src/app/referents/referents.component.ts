import { Component, OnInit, OnDestroy, ViewChild, ViewChildren } from '@angular/core';

import { Subscription } from 'rxjs';

import {MatTableDataSource} from '@angular/material/table';
import {MatPaginator} from '@angular/material/paginator';
import {MatSort} from '@angular/material/sort';

import { ReferentsService } from "@shared/api-swagger/api/api";

import { CollaborateurModel } from "@shared/api-swagger/model/collaborateurModel";
import { DisplayCollaborateur } from "@shared/displayInfo/displays";


/**
    */
@Component({
  selector: 'app-referents',
  templateUrl: './referents.component.html'
})
export class ReferentsComponent implements OnInit {

  referentsDiponibles : DisplayCollaborateur[];
  referentsFiltre : DisplayCollaborateur[];
  private referentsDisponiblesSubscription : Subscription;

  displayedColumns : string[] = ["agence", "referent"];

  dataSource : MatTableDataSource<DisplayCollaborateur>;

  /**
    * contenu de la recherche.
    */
  search = "";

  /**
    * Pagination du tableau.
    */
  @ViewChild(MatPaginator) paginator: MatPaginator;

  /**
    * Tri par les éléments du tableau selon la colonne choisie.
    */
  @ViewChild(MatSort) sort: MatSort;

  /**
    * Spécifie si la liste des EP est en cours de chargement et d'écriture dans le tableau.
    */
  chargement = true;
  constructor(private service: ReferentsService) {}

  ngOnInit() {
    this.referentsDisponiblesSubscription = this.service.getReferents(undefined, undefined, 1).subscribe(
      referents => { this.initReferents(referents); }
    );
  }

  initReferents(referents:CollaborateurModel[]) {
    this.referentsDiponibles = [];
    let referentDisplay: DisplayCollaborateur;
    for(let r of referents) {
      referentDisplay = new DisplayCollaborateur();
      referentDisplay.id = r.id;
      referentDisplay.prenom = r.prenom;
      referentDisplay.nom = r.nom;
      referentDisplay.agence = r.businessUnit.nom;
      this.referentsDiponibles.push(referentDisplay);
    }
    this.referentsFiltre = this.referentsDiponibles;
    this.dataSource = new MatTableDataSource(this.referentsFiltre);
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
  }

  ngOnDestroy() {
    if(this.referentsDisponiblesSubscription != undefined) {
      this.referentsDisponiblesSubscription.unsubscribe();
    }
  }
}
