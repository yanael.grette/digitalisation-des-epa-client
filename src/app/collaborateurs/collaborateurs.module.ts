import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';

import { MaterialModule } from "@shared/angular-material/angular-material.module";

import {NavMenuModule} from '@shared/nav-menu/nav-menu.module';

import { CollaborateursComponent } from "./collaborateurs.component";
import { DetailsCollaborateurComponent } from "./details-collaborateur/details-collaborateur.component";
import { EvaluationComponent } from "./formations-collaborateur/details-evaluation/evaluation.component";
import { EditEvaluationComponent } from "./formations-collaborateur/edit-evaluation/edit-evaluation.component";
import { FormationsCollaboateurComponent } from "./formations-collaborateur/formations-collaborateur.component";

import { CollaborateursRoutingModule } from "./collaborateurs.routing.module";


@NgModule({
  declarations: [
    DetailsCollaborateurComponent, EvaluationComponent, EditEvaluationComponent,
    CollaborateursComponent, FormationsCollaboateurComponent
  ],
  exports: [
    CollaborateursComponent
  ],
  imports: [
    //FormsModule,
    CommonModule,
    MaterialModule,
    NavMenuModule,
    CollaborateursRoutingModule,
    RouterModule
  ],
})
export class CollaborateursModule {}
