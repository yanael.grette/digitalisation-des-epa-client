import { Component, OnInit, ViewChild, OnDestroy } from '@angular/core';

import { Observable, Subscription } from 'rxjs';
import {ActivatedRoute} from '@angular/router';

import {MatTableDataSource} from '@angular/material/table';
import {MatPaginator} from '@angular/material/paginator';
import {MatSort} from '@angular/material/sort';

import { CollaborateursService, EpService } from "@shared/api-swagger/api/api";

import { CollaborateurModel } from "@shared/api-swagger/model/collaborateurModel";
import { EpModel } from "@shared/api-swagger/model/epModel";
import { DisplayEP } from "@shared/displayInfo/displays";

/**
    */
@Component({
  selector: 'app-details-collaborateur',
  templateUrl: './details-collaborateur.component.html'
})
export class DetailsCollaborateurComponent implements OnInit {
//epCollaborateurIdCollaborateurGet
  collaborateur: CollaborateurModel;
  private collaborateurSubscription : Subscription;
  private epSubscription : Subscription;
  epEffectues : DisplayEP[];
  displayedColumns: string[] = ["dateentretien", "referent", "type", "details"];
  dataSource : MatTableDataSource<DisplayEP>;
  eploaded = false;
  idCollaborateur: any;

  /**
    * Pagination du tableau.
    */
  @ViewChild(MatPaginator) paginator: MatPaginator;

  /**
    * Tri par les éléments du tableau selon la colonne choisie.
    */
  @ViewChild(MatSort) sort: MatSort;

  constructor(private collaborateusrService:CollaborateursService, private epService:EpService, private route: ActivatedRoute) {}

  ngOnInit() {
    this.idCollaborateur = this.route.snapshot.paramMap.get('id');
    this.collaborateurSubscription = this.collaborateusrService.getCollaborateurById(this.idCollaborateur).subscribe(
      collaborateur => this.initCollaborateur(collaborateur[0]),
      err => console.log(err)
    );
  }

  initCollaborateur(collaborateur:CollaborateurModel) {
    this.collaborateur = collaborateur;
    this.epSubscription = this.epService.getEPByCollaborateur(this.idCollaborateur).subscribe(
      ep => this.initEP(ep)
    )
  }

  initEP(ep:EpModel[]) {
    this.epEffectues = [];
    let epDisplay : DisplayEP;
    for(let un_ep of ep) {
      epDisplay = new DisplayEP();
      epDisplay.id = un_ep.id;
      epDisplay.referent = un_ep.referent;
      epDisplay.etat = un_ep.etat;
      epDisplay.type = un_ep.type;
      epDisplay.dateentretien = un_ep.dateEntretien;
      this.epEffectues.push(epDisplay);
    }
    this.dataSource = new MatTableDataSource(this.epEffectues);
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
    this.eploaded = true;
  }

  ngOnDestroy() {
    if(this.epSubscription!= undefined) {
      this.epSubscription.unsubscribe();
    }
    if(this.collaborateurSubscription!= undefined) {
      this.collaborateurSubscription.unsubscribe();
    }
  }
}
