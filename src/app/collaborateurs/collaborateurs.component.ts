import { Component, OnInit, OnDestroy, ViewChild, ViewChildren } from '@angular/core';

import { Observable, Subscription } from 'rxjs';

import {MatTableDataSource} from '@angular/material/table';
import {MatPaginator} from '@angular/material/paginator';
import {MatSort} from '@angular/material/sort';

import { CollaborateursService } from "@shared/api-swagger/api/api";

import { CollaborateurModel } from "@shared/api-swagger/model/models";
import { DisplayCollaborateur } from "@shared/displayInfo/displays";

/**
    * Composant qui sert à l'affichage de la liste des collaborateurs en fonction de l'agence de son utilitateur.
    * Seuls les commerciaux, RH et assistantes pourront accéder à la liste des collaborateurs.
    * Les données affichées : Agence-Nom Prénom-Date Embauche-Responsable Commercial-Date Prochain EP
    */
@Component({
  selector: 'app-collaborateurs',
  templateUrl: './collaborateurs.component.html'
})
export class CollaborateursComponent implements OnInit {

  collaborateursDisponibles : DisplayCollaborateur[];
  collaborateursFiltre : DisplayCollaborateur[];
  private collaborateursDisponiblesSubscription : Subscription;

  displayedColumns : string[] = ["agence", "collaborateur", "dateembauche", "referent"];

  dataSource : MatTableDataSource<DisplayCollaborateur>;

  /**
    * contenu de la recherche.
    */
  search = "";

  /**
    * Pagination du tableau.
    */
  @ViewChild(MatPaginator) paginator: MatPaginator;

  /**
    * Tri par les éléments du tableau selon la colonne choisie.
    */
  @ViewChild(MatSort) sort: MatSort;

  /**
    * Spécifie si la liste des EP est en cours de chargement et d'écriture dans le tableau.
    */
  chargement = true;
  constructor(private service: CollaborateursService) {}

  ngOnInit() {
    this. collaborateursDisponiblesSubscription = this.service.getCollaborateurs(undefined,undefined,undefined,1).subscribe(
      collaborateurs => {
        this.initCollaborateur(collaborateurs)
      }
    );
  }

  initCollaborateur(collaborateurs:CollaborateurModel[]) {
    this.collaborateursDisponibles = [];
    let collaborateurDisplay : DisplayCollaborateur;
    let today = new Date();
    console.log(collaborateurs);
    for(let c of collaborateurs) {
      collaborateurDisplay = new DisplayCollaborateur();
      collaborateurDisplay.id = c.id;
      collaborateurDisplay.prenom = c.prenom;
      collaborateurDisplay.nom = c.nom;
      collaborateurDisplay.agence = c.businessUnit.nom;
      collaborateurDisplay.embauche = c.dateArrivee;
      collaborateurDisplay.anciennete = this.setAnciennete(new Date(c.dateArrivee), today);
      collaborateurDisplay.annee = Math.floor(collaborateurDisplay.anciennete / 31536000000);
      collaborateurDisplay.mois = Math.floor(collaborateurDisplay.anciennete / 2629800000 % 12);
      this.collaborateursDisponibles.push(collaborateurDisplay);
    }
    this.collaborateursFiltre = this.collaborateursDisponibles;
    this.dataSource = new MatTableDataSource(this.collaborateursFiltre);
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
  }


  setAnciennete(firstDate, secondDate) {
    return Math.abs(firstDate-secondDate);
  }

  ngOnDestroy() {
    if(this.collaborateursDisponiblesSubscription != undefined) {
      this.collaborateursDisponiblesSubscription.unsubscribe();
    }
  }
}
