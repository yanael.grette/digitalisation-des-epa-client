import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';


import { CollaborateursComponent } from "./collaborateurs.component";
import { DetailsCollaborateurComponent } from "./details-collaborateur/details-collaborateur.component";
import { FormationsCollaboateurComponent } from "./formations-collaborateur/formations-collaborateur.component";
import { EvaluationComponent } from './formations-collaborateur/details-evaluation/evaluation.component';
import { EditEvaluationComponent } from './formations-collaborateur/edit-evaluation/edit-evaluation.component';

import { paths_collaborateurs } from '@shared/utils/paths';

import { KeycloakGuard } from '@shared/guards/keycloakguard';


const routes: Routes = [
  { path:'', component: CollaborateursComponent, pathMatch: 'full', canActivate: [KeycloakGuard] },
  { path:paths_collaborateurs.formations, component: FormationsCollaboateurComponent, canActivate: [KeycloakGuard] },
  { path:paths_collaborateurs.evaluation, component: EvaluationComponent, canActivate: [KeycloakGuard] },
  { path:paths_collaborateurs.edit, component: EditEvaluationComponent, canActivate: [KeycloakGuard] },
  { path:paths_collaborateurs.get, component: DetailsCollaborateurComponent, canActivate: [KeycloakGuard] }
];


@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CollaborateursRoutingModule {}
