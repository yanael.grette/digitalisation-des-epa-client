import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';


import { EpSaisieComponent } from "./ep-saisie.component";
import { EpsSaisieComponent } from "./eps-saisie/eps-saisie.component";
import { EpaSaisieComponent } from "./epa-saisie/epa-saisie.component";
import { EpaSixAnsSaisieComponent } from "./epa-six-ans-saisie/epa-six-ans-saisie.component";

import { KeycloakGuard } from '@shared/guards/keycloakguard';

import { paths_saisie_ep } from "@shared/utils/paths";

const routes: Routes = [
  { path:'',
    component: EpSaisieComponent,
    canActivate: [KeycloakGuard],
    children: [
      { path:paths_saisie_ep.epa, component: EpaSaisieComponent, canActivate: [KeycloakGuard] },
      { path:paths_saisie_ep.eps, component: EpsSaisieComponent, canActivate: [KeycloakGuard] },
      { path:paths_saisie_ep.epa6ans, component: EpaSixAnsSaisieComponent, canActivate: [KeycloakGuard] }
    ]
  }
];

/*
/saisie-ep
/saisie-ep/eps
/saisie-ep/epa
/saisie-ep/epasixans
*/

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class EpSaisieRoutingModule {}
