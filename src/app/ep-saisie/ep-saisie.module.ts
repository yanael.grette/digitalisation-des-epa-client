import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router'

import { MaterialModule } from "@shared/angular-material/angular-material.module";

import {NavMenuModule} from '@shared/nav-menu/nav-menu.module';

import { EpSaisieComponent } from './ep-saisie.component';
import { EpaSaisieComponent } from './epa-saisie/epa-saisie.component';
import { EpsSaisieComponent } from './eps-saisie/eps-saisie.component';
import { EpaSixAnsSaisieComponent } from './epa-six-ans-saisie/epa-six-ans-saisie.component';

import { EpSaisieRoutingModule } from './ep-saisie.routing.module';

@NgModule({
  declarations: [EpSaisieComponent, EpsSaisieComponent, EpaSaisieComponent,
    EpaSixAnsSaisieComponent
  ],
  exports: [EpSaisieComponent
  ],
  imports: [
    //BrowserAnimationsModule,
    //FormsModule,
    MaterialModule,
    NavMenuModule,
    EpSaisieRoutingModule,
    RouterModule
  ],
})
export class EpSaisieModule {}
