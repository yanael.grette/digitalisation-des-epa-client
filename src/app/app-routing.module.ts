import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { Routes } from '@angular/router';

import { HomeComponent } from './home/';

import { KeycloakGuard } from '@shared/guards/keycloakguard';

import { paths_collaborateurs, paths_demandes_delegation, paths_demandes_formation,
  paths_ep, paths_saisie_ep, paths_formation, paths_home, paths_referents } from '@shared/utils/paths';

const routes: Routes = [
  {
    path: '',
    redirectTo: paths_home.path,
    pathMatch: 'full'
  },
  {
    path: paths_home.path,
    component: HomeComponent,
    canActivate: [KeycloakGuard],
    pathMatch: 'full'
  },
  {
    path: paths_collaborateurs.path,
    loadChildren: () => import('./collaborateurs/collaborateurs.module').then(m=> m.CollaborateursModule)
  },
  {
    path: paths_demandes_delegation.path,
    loadChildren: () => import('./demandes-delegation/demandes-delegation.module').then(m=> m.DemandesDelegationModule)
  },
  {
    path: paths_demandes_formation.path,
    loadChildren: () => import('./demandes-formation/demandes-formation.module').then(m=> m.DemandesFormationModule)
  },
  {
    path: paths_ep.path,
    loadChildren: () => import('./ep/ep.module').then(m=> m.EpModule)
  },
  {
    path: paths_saisie_ep.path,
    loadChildren: () => import('./ep-saisie/ep-saisie.module').then(m=> m.EpSaisieModule)
  },
  {
    path: paths_formation.path,
    loadChildren: () => import('./formations/formations.module').then(m=> m.FormationsModule)
  },
  {
    path: paths_referents.path,
    loadChildren: () => import('./referents/referents.module').then(m=> m.ReferentsModule)
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
  providers: [KeycloakGuard]
})
export class AppRoutingModule {}
