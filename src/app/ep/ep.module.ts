import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';

import { MaterialModule } from "@shared/angular-material/angular-material.module";

import {NavMenuModule} from '@shared/nav-menu/nav-menu.module';

import { EpComponent } from './ep.component';
import { EpAugmentationSalaireComponent } from "./ep-augmentation-salaire/ep-augmentation-salaire.component";
import { EpChoixDateComponent } from "./ep-choix-date/ep-choix-date.component";
import { EpDemandeDelegationComponent } from "./ep-demande-delegation/ep-demande-delegation.component";
import { EpDemandesFormationComponent } from "./ep-demandes-formation/ep-demandes-formation.component";
import { EpPropositionsDatesComponent } from "./ep-propositions-dates/ep-propositions-dates.component";
import { EpParticipantsComponent } from "./ep-participants/ep-participants.component";
import { NewParticipantComponent } from "./ep-participants/new-participant/new-participant.component";
import { EpConsultationComponent } from "./ep-consultation/ep-consultation.component";
import { EpSignatureComponent } from "./ep-signature/ep-signature.component";
import { EpaComponent } from "./epa/epa.component";
import { EpaSixAnsComponent } from "./epa-six-ans/epa-six-ans.component";
import { EpsComponent } from "./eps/eps.component";
import { EpCommentaireAssistantComponent } from "./ep-commentaire-assistant/ep-commentaire-assistant.component";
import { EpCommentaireReferentComponent } from "./ep-commentaire-referent/ep-commentaire-referent.component";

import { EpRoutingModule } from './ep.routing.module';

/**/
@NgModule({
  declarations: [EpComponent, EpAugmentationSalaireComponent, EpChoixDateComponent,
    EpDemandeDelegationComponent, EpDemandesFormationComponent, EpParticipantsComponent,
    EpPropositionsDatesComponent, NewParticipantComponent, EpConsultationComponent,
    EpSignatureComponent, EpaComponent, EpaSixAnsComponent, EpsComponent,
    EpCommentaireAssistantComponent, EpCommentaireReferentComponent
  ],
  exports: [EpComponent
  ],
  imports: [
    MaterialModule,
    NavMenuModule,
    EpRoutingModule,
    RouterModule
  ],
})
export class EpModule {}
