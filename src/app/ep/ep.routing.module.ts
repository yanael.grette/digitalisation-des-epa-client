import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';


import { EpComponent } from "./ep.component";
import { EpAugmentationSalaireComponent } from "./ep-augmentation-salaire/ep-augmentation-salaire.component";
import { EpChoixDateComponent } from "./ep-choix-date/ep-choix-date.component";
import { EpConsultationComponent } from './ep-consultation/ep-consultation.component';
import { EpDemandeDelegationComponent } from "./ep-demande-delegation/ep-demande-delegation.component";
import { EpDemandesFormationComponent } from "./ep-demandes-formation/ep-demandes-formation.component";
import { EpParticipantsComponent } from "./ep-participants/ep-participants.component";
import { EpPropositionsDatesComponent } from "./ep-propositions-dates/ep-propositions-dates.component";
import { EpSignatureComponent } from "./ep-signature/ep-signature.component";
import { EpaComponent } from "./epa/epa.component";
import { EpaSixAnsComponent } from "./epa-six-ans/epa-six-ans.component";
import { EpsComponent } from "./eps/eps.component";
import { EpCommentaireAssistantComponent } from "./ep-commentaire-assistant/ep-commentaire-assistant.component";
import { EpCommentaireReferentComponent } from "./ep-commentaire-referent/ep-commentaire-referent.component";
import { NewParticipantComponent } from "./ep-participants/new-participant/new-participant.component";

import { KeycloakGuard } from '@shared/guards/keycloakguard';
import { paths_ep } from "@shared/utils/paths";


const routes: Routes = [
  {
    path:'',
    component: EpComponent,
    canActivate: [KeycloakGuard]
  },
  {
    path:paths_ep.consultation,
    component: EpConsultationComponent,
    canActivate: [KeycloakGuard],
    children: [
      {path:paths_ep.salaire, component: EpAugmentationSalaireComponent, canActivate: [KeycloakGuard]},
      {path:paths_ep.choixdate, component: EpChoixDateComponent, canActivate: [KeycloakGuard]},
      {path:paths_ep.demandedelegation, component: EpDemandeDelegationComponent, canActivate: [KeycloakGuard]},
      {path:paths_ep.demandesformation, component: EpDemandesFormationComponent, canActivate: [KeycloakGuard]},
      {path:paths_ep.participants, component: EpParticipantsComponent, canActivate: [KeycloakGuard]},
      {path:paths_ep.propositionsdates, component: EpPropositionsDatesComponent, canActivate: [KeycloakGuard]},
      {path:paths_ep.signature, component: EpSignatureComponent, canActivate: [KeycloakGuard]},
      {path:paths_ep.epa, component: EpaComponent, canActivate: [KeycloakGuard]},
      {path:paths_ep.eps, component: EpsComponent, canActivate: [KeycloakGuard]},
      {path:paths_ep.epa6ans, component: EpaSixAnsComponent, canActivate: [KeycloakGuard]},
      {path:paths_ep.assistant, component: EpCommentaireAssistantComponent, canActivate: [KeycloakGuard]},
      {path:paths_ep.referent, component: EpCommentaireReferentComponent, canActivate: [KeycloakGuard]}
    ]
  },
  {
    path:paths_ep.newparticipant,
    component: NewParticipantComponent,
    canActivate: [KeycloakGuard]
  },
];


@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class EpRoutingModule {}
