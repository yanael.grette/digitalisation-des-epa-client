import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';


import { DemandesDelegationComponent } from "./demandes-delegation.component";
import { DemandeDelegationComponent } from "./details-demande-delegation/demande-delegation.component";

import { KeycloakGuard } from '@shared/guards/keycloakguard';

import { paths_demandes_delegation } from "@shared/utils/paths";

const routes: Routes = [
  {
    path:'',
    component: DemandesDelegationComponent,
    pathMatch: 'full',
    canActivate: [KeycloakGuard]
  },
  {
    path: paths_demandes_delegation.get,
    component: DemandeDelegationComponent,
    canActivate: [KeycloakGuard]
  }
];


@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DemandesDelegationRoutingModule {}
