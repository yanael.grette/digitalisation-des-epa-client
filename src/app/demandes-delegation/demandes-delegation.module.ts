import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';

import { MaterialModule } from "@shared/angular-material/angular-material.module";

import {NavMenuModule} from '@shared/nav-menu/nav-menu.module';


import { DemandesDelegationComponent } from './demandes-delegation.component';
import { DemandeDelegationComponent } from './details-demande-delegation/demande-delegation.component';
import { DemandesDelegationRoutingModule } from './demandes-delegation.routing.module';


@NgModule({
  declarations: [ DemandesDelegationComponent, DemandeDelegationComponent
  ],
  exports: [ DemandesDelegationComponent
  ],
  imports: [
    MaterialModule,
    NavMenuModule,
    DemandesDelegationRoutingModule,
    RouterModule

  ],
})
export class DemandesDelegationModule {}
