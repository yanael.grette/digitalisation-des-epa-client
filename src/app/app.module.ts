import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgModule, DoBootstrap } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { KeycloakAngularModule, KeycloakService } from 'keycloak-angular';
import { RouterModule } from '@angular/router';

import { registerLocaleData } from '@angular/common';
import localeFr from '@angular/common/locales/fr';
registerLocaleData(localeFr, 'fr');


import { AppComponent } from './app.component';

import { AppRoutingModule } from './app-routing.module';
import { ApiModule } from '@shared/api-swagger'

import { HomeModule } from './home';
import { CollaborateursModule } from './collaborateurs';
import { ReferentsModule } from './referents';
import { FormationsModule } from './formations';
import { DemandesFormationModule } from './demandes-formation';
import { DemandesDelegationModule } from './demandes-delegation';
import { EpSaisieModule } from "./ep-saisie";
import { EpModule } from "./ep"

import { environment } from '@env';



/**
  * constante Keycloak qui pourra être utilisé dans tout le projet.
  */
let keycloakService: KeycloakService = new KeycloakService();

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule, BrowserAnimationsModule,
    KeycloakAngularModule, AppRoutingModule,
    HttpClientModule, ApiModule,
    HomeModule, CollaborateursModule,
    ReferentsModule, FormationsModule,
    DemandesFormationModule, DemandesDelegationModule,
    EpSaisieModule
  ],
  providers: [
    {
      provide: KeycloakService,
      useValue: keycloakService
    }
  ],
  entryComponents: [AppComponent]
})

export class AppModule implements DoBootstrap {
  async ngDoBootstrap(app) {
    const { keycloakConfig } = environment;

    try {
      await keycloakService.init({ config: keycloakConfig });
      app.bootstrap(AppComponent);

    } catch (error) {
      console.error('Keycloak init failed', error);
    }
  }
}
