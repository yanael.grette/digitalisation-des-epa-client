import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';


import { DemandesFormationComponent } from "./demandes-formation.component";
import { DemandeFormationComponent } from "./details-demande-formation/demande-formation.component";
import { NewDemandeFormationComponent } from "./new-demande-formation/new-demande-formation.component";

import { KeycloakGuard } from '@shared/guards/keycloakguard';

import { paths_demandes_formation } from "@shared/utils/paths";

const routes: Routes = [
  { path:'', component: DemandesFormationComponent, pathMatch: 'full', canActivate: [KeycloakGuard] },
  { path:paths_demandes_formation.new, component: NewDemandeFormationComponent, canActivate: [KeycloakGuard] },
  { path:paths_demandes_formation.get, component: DemandeFormationComponent, canActivate: [KeycloakGuard] }

];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DemandesFormationsRoutingModule {}
