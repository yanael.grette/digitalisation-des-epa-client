import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';


import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";

import { MaterialModule } from "@shared/angular-material/angular-material.module";

import {NavMenuModule} from '@shared/nav-menu/nav-menu.module';

import { DemandesFormationComponent } from './demandes-formation.component';
import { DemandeFormationComponent } from './details-demande-formation/demande-formation.component'
import { NewDemandeFormationComponent } from './new-demande-formation/new-demande-formation.component'
import { DemandesFormationsRoutingModule } from './demandes-formation.routing.module';

@NgModule({
  declarations: [ DemandesFormationComponent, DemandeFormationComponent,
    NewDemandeFormationComponent
  ],
  exports: [
      DemandesFormationComponent
  ],
  imports: [
    MaterialModule,
    NavMenuModule,
    DemandesFormationsRoutingModule,
    RouterModule
  ],
})
export class DemandesFormationModule {}
