import { Component, OnInit } from '@angular/core';

import { Subscription } from 'rxjs';
import {ActivatedRoute} from '@angular/router';

import {MatTableDataSource} from '@angular/material/table';
import {MatPaginator} from '@angular/material/paginator';
import {MatSort} from '@angular/material/sort';

import { FormationsService } from '@shared/api-swagger/api/api';
import { FormationModel } from "@shared/api-swagger/model/models";
import { DisplayParticipation } from "@shared/displayInfo/displays";

/**
    */
@Component({
  selector: 'app-formation',
  templateUrl: './formation.component.html'
})
export class FormationComponent implements OnInit {
  statutEnum = FormationModel.StatutEnum;
  formation:FormationModel;
  dateTexte:string = "Prévue le";
  formationSubscription: Subscription;

  participationsDisplay: DisplayParticipation[];

  dataSource : MatTableDataSource<DisplayParticipation>;
  displayedColumns: string[]= ["agence", "collaborateur", "dateinscription", "ep"];

  id:any
  constructor(private service:FormationsService,private route:ActivatedRoute) {}

  ngOnInit() {
    this.id = this.route.snapshot.paramMap.get('id');
    this.formationSubscription = this.service.getFormationById(this.id).subscribe(
      formation => this.initFormation(formation)
    );
  }

  initFormation(formation:FormationModel) {
    this.formation = formation;
    if(this.formation.statut == FormationModel.StatutEnum.Realise) {
      this.dateTexte = "Effecutée le";
      this.displayedColumns.push("evaluation");
    }
    if(this.formation.statut == FormationModel.StatutEnum.Annule) {
      this.dateTexte = "Initialement prévue le";
    }
    if(formation.participantsFormation != undefined && formation.participantsFormation.length != 0 ) {
      this.initParticpationFormation();
    }
  }


  initParticpationFormation() {
    let participationDisplay : DisplayParticipation;
    this.participationsDisplay = [];
    for(let participation of this.formation.participantsFormation ) {
      participationDisplay = new DisplayParticipation();
      participationDisplay.id =participation.id;
      participationDisplay.dateCreation = participation.dateCreation;

      participationDisplay.estEvaluee = participation.estEvaluee;
      participationDisplay.idEP = participation.demandeformation.ep.id;
      participationDisplay.statutEP = participation.demandeformation.ep.etat;
      participationDisplay.idCollaborateur = participation.demandeformation.ep.collaborateur.id;
      participationDisplay.collaborateur = participation.demandeformation.ep.collaborateur.prenom + " " + participation.demandeformation.ep.collaborateur.nom;
      participationDisplay.agence = participation.demandeformation.ep.collaborateur.businessUnit.nom;

      this.participationsDisplay.push(participationDisplay);
    }
    this.dataSource = new MatTableDataSource(this.participationsDisplay);
  }

  ngOnDestroy() {
    if(this.formationSubscription != undefined) {
      this.formationSubscription.unsubscribe();
    }
  }
}
