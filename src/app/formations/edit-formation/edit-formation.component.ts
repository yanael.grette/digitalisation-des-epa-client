import { Component, OnInit } from '@angular/core';

import { Observable, Subscription } from 'rxjs';
import { Router, ActivatedRoute } from '@angular/router';
import { FormBuilder, FormGroup, FormControl, Validators } from '@angular/forms';


import {MatTableDataSource} from '@angular/material/table';
import {MatPaginator} from '@angular/material/paginator';
import {MatSort} from '@angular/material/sort';


import { FormationModel } from '@shared/api-swagger/model/models';
import { FormationsService } from '@shared/api-swagger/api/api';

/**
    */
@Component({
  selector: 'app-edit-formation',
  templateUrl: './edit-formation.component.html',
  styleUrls: ["edit-formation.component.css"]
})
export class EditFormationComponent implements OnInit {
  formation:FormationModel;
  formationSubscription: Subscription;
  formationForm: FormGroup;
  id :any;
  statuts = FormationModel.StatutEnum;
  modes = FormationModel.ModeEnum;
  types = FormationModel.TypeEnum;
  keysStatuts: any[];
  keysModes: any[];
  keysTypes: any[];

  constructor(private service: FormationsService, private fb: FormBuilder,
    private activatedRoute:ActivatedRoute, private router: Router) {}

  ngOnInit() {
    this.id = this.activatedRoute.snapshot.paramMap.get('id');
    this.formationSubscription = this.service.getFormationById(this.id).subscribe(
      formation => this.initFormation(formation)
    );
    this.keysStatuts = Object.keys(this.statuts).filter(String);
    this.keysModes = Object.keys(this.modes).filter(String);
    this.keysTypes = Object.keys(this.types).filter(String);
  }

  initFormation(formation:FormationModel) {
    this.formation = formation;
    this.formationForm= this.fb.group(
      {

        intitule: [this.formation.intitule],
        origine: [this.formation.origine],
        statut : [this.formation.statut],
        dateDebut: [new Date(this.formation.dateDebut)],
        dateFin: [new Date(this.formation.dateFin)],
        heure: [this.formation.heure],
        jour: [this.formation.jour],
        organisme: [this.formation.organisme],
        mode: [this.formation.mode],
        type: [this.formation.type],
        estCertifie: [this.formation.estCertifie]
      });
  }

  updateFormation() {
    this.formation = this.formationForm.value;
    this.formation.id = this.id;
    this.formationSubscription = this.service.updateFormation(this.formation).subscribe(
      response => {
        this.router.navigate(['/formations',this.id]);
      }
    );
  }

  supprimerFormation() {
    this.formationSubscription = this.service.deleteFormation(this.formation.id).subscribe(
      response => this.router.navigate([""])
    );
  }

  ngOnDestroy() {
    if(this.formationSubscription != undefined) {
      this.formationSubscription.unsubscribe();
    }
  }
}
