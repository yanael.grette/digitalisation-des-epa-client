import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { RouterModule } from '@angular/router';

import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { MaterialModule } from "@shared/angular-material/angular-material.module";

import {NavMenuModule} from '@shared/nav-menu/nav-menu.module';

import { FormationsComponent } from './formations.component';
import { NewFormationComponent } from './new-formation/new-formation.component';
import { FormationComponent } from './details-formation/formation.component';
import { EditFormationComponent } from './edit-formation/edit-formation.component';

import { FormationsRoutingModule } from './formations.routing.module';

@NgModule({
  declarations: [ FormationsComponent, NewFormationComponent,
    FormationComponent, EditFormationComponent
  ],
  exports: [
      FormationsComponent
  ],
  imports: [
    CommonModule,
    MaterialModule,
    NavMenuModule,
    RouterModule,
    FormationsRoutingModule,
    ReactiveFormsModule
  ],
})
export class FormationsModule {}
