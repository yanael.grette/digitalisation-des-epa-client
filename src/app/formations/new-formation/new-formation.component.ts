import { Component, OnInit } from '@angular/core';
import { Observable, Subscription } from 'rxjs';
import { Router } from '@angular/router';
import { FormBuilder, FormGroup, FormControl, Validators } from '@angular/forms';

import { FormationModel } from "@shared/api-swagger/model/models";

import { FormationsService } from "@shared/api-swagger/api/api";

/**
    */
@Component({
  selector: 'app-new-formation',
  templateUrl: './new-formation.component.html',
  styleUrls: ['./new-formation.component.css']
})
export class NewFormationComponent implements OnInit {
  formationSubscription: Subscription;
  nouvelleformation: FormationModel;
  formationForm = this.fb.group(
    {
      intitule: [""],
      origine: [""],
      statut : [FormationModel.StatutEnum.Planifie],
      dateDebut: [""],
      dateFin: [""],
      heure: [""],
      jour: [""],
      organisme: [""],
      mode: [""],
      type: [""],
      estCertifie: [""]
    }
  );
  modes = FormationModel.ModeEnum;
  types = FormationModel.TypeEnum;
  keysModes: any[];
  keysTypes: any[];

  constructor(private fb: FormBuilder, private service:FormationsService, private router: Router) { }

  ngOnInit() {
    this.keysModes = Object.keys(this.modes).filter(String);
    this.keysTypes = Object.keys(this.types).filter(String);
  }

  ajouterFormation() {
    this.nouvelleformation = this.formationForm.value;
    this.formationSubscription = this.service.ajouterFormation(this.nouvelleformation).subscribe(
      response => {
        this.router.navigate(['/formations',response["id"]]);
      }
    );
  }

  ngOnDestroy() {
    if(this.formationSubscription != undefined) {
      this.formationSubscription.unsubscribe();
    }
  }
}
