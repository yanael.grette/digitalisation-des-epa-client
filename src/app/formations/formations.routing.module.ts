import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';


import { FormationsComponent } from "./formations.component";
import { FormationComponent } from "./details-formation/formation.component";
import { EditFormationComponent } from "./edit-formation/edit-formation.component";
import { NewFormationComponent } from "./new-formation/new-formation.component";

import { KeycloakGuard } from '@shared/guards/keycloakguard';

import { paths_formation } from "@shared/utils/paths";

const routes: Routes = [
  { path:'', component: FormationsComponent, pathMatch: 'full', canActivate: [KeycloakGuard] },
  { path:paths_formation.edit, component: EditFormationComponent, canActivate: [KeycloakGuard] },
  { path:paths_formation.new, component: NewFormationComponent, canActivate: [KeycloakGuard] },
  { path:paths_formation.get, component: FormationComponent, canActivate: [KeycloakGuard] }
];


@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class FormationsRoutingModule {}
