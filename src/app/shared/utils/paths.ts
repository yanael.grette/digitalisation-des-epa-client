const paths_collaborateurs = {
  base: "/collaborateurs",
  path: "collaborateurs",
  get: ":id",
  formations: ":id/formations",
  evaluation: ":id/formations/evaluation/:idParticipation",
  edit: ":id/formations/evaluation/:id/edit"
};

const paths_demandes_delegation = {
  base: "/demandesdelegation",
  path: "demandesdelegation",
  get: ":id"
};

const paths_demandes_formation = {
  base: "/demandesformation",
  path: "demandesformation",
  get: ":id",
  new: "nouvelledemandeformation"
};

const paths_ep = {
  base: "/ep",
  path: "ep",
  salaire: "augmentationsalaire",
  choixdate: "choixdate",
  demandedelegation: "demandedelegation",
  demandesformation: "demandesformation",
  propositionsdates: "propositiondate",
  participants: "participants",
  commentaireAssistante: "commentaireassistante",
  commentaireReferent: "commentairereferent",
  signature: "signature",
  epa: "epa",
  eps: "eps",
  epa6ans: "epasixans",
  assistant: "commentaireassistant",
  referent: "commentairereferent",
  consultation: ":id",
  newparticipant: "ajoutparticipant"
};

const paths_saisie_ep = {
  base: "/saisieep",
  path: "saisieep",
  epa: "epa",
  eps: "eps",
  epa6ans: "epasixans"
};

const paths_formation = {
  base: "/formations",
  path: "formations",
  get: ":id",
  edit: ":id/edit",
  //evaluation: ":id/evaluation/ideval",
  new: "nouvelleformation"
};

const paths_home = {
  base: "/home",
  path: "home"
};


const paths_referents = {
  base: "/referents",
  path: "referents",
  get: ":id"
};

export { paths_collaborateurs, paths_demandes_delegation, paths_demandes_formation,
  paths_ep, paths_saisie_ep, paths_formation, paths_home, paths_referents};
