/**
  * Enumération des rôles utilisateur dans Keycloak.
  */
export enum Role {
  collaborateur = "Collaborateur",
  assistante = "Assistante",
  rh = "RH",
  commercial = "Commercial"
}
