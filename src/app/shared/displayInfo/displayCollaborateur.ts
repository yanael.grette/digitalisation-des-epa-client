export class DisplayCollaborateur {
  id: string;
  agence: string;
  nom: string;
  prenom: string;
  embauche : Date;
  anciennete : number;
  annee: number;
  mois: number;
}
