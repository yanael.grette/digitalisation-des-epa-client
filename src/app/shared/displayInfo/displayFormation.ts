import { FormationModel } from "@shared/api-swagger/model/models";

export class DisplayFormation {
  id: number;
  intitule: string;
  nbParticipants : number;
  datePrevu: Date;
  origine : string;
  statut: FormationModel.StatutEnum;
}
