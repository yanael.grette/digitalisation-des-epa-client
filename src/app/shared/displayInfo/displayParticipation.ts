import { CollaborateurModel, EpModel, FormationModel } from "@shared/api-swagger/model/models";

export class DisplayParticipation {
  id: number;
  intitule: string;
  agence: string;
  collaborateur: string;
  idCollaborateur: string;
  idEP: number;
  estEvaluee: boolean;
  statutEP : number;
  dateCreation: Date;
  formation: FormationModel;
}
