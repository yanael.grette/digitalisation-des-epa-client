/**
  * Class pour récupérer les informations d'un EP qui seront affichées et/ou utilisées.
  */

import {CollaborateurModel} from "../api-swagger/model/collaborateurModel";

export class DisplayEP {
  id : number;
  agence : string;
  collaborateur : CollaborateurModel;
  referent : CollaborateurModel;
  type : string;
  etat : number;
  datemail : Date;
  dateentretien : Date;
  anciennete : number;
  annee: number;
  mois: number;
}
