import { Component } from "@angular/core";

/**
  * Le nav-menu qui sera affiché pour les commerciaux.
  * La navigation des commerciaux : accès liste des collaborateurs, accès liste EP, accès liste des formations.
  */
@Component({
  selector : "app-nav-menu-commercial",
  templateUrl : "./nav-menu-commercial.component.html"
})
export class NavMenuCommercialComponent {}
