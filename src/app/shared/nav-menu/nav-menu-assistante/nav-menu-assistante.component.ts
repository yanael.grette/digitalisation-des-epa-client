import { Component } from "@angular/core";
import { CommonModule } from "@angular/common";
/**
  * Le nav-menu qui sera affiché pour l'assistante.
  * La navigation des assistantes : accès à la liste des référents, accès à la liste des collaborateurs, accès à la liste de tous les EP.
  */
@Component({
  selector : "app-nav-menu-assistante",
  templateUrl : "./nav-menu-assistante.component.html"
})
export class NavMenuAssistanteComponent {}
