import { Component } from "@angular/core";

/**
  * Le nav-menu qui sera affiché pour les RH.
  * La navigation des RH : accès liste collaborateurs, accès liste demandes de formation, accès liste formations, accès création formation, accès création demande de formation.
  */
@Component({
  selector : "app-nav-menu-rh",
  templateUrl : "./nav-menu-rh.component.html"
})
export class NavMenuRHComponent {}
