import { Component } from "@angular/core";

/**
  * Le nav-menu qui sera affiché pour les collaborateurs.
  * La navigation des collaborateurs : accès à leur liste d'EP, accès à leur liste de formation.
  * Cas des référents : accès aux demandesd de délégation.
  */
@Component({
  selector : "app-nav-menu-collaborateur",
  templateUrl : "./nav-menu-collaborateur.component.html"
})
export class NavMenuCollaborateurComponent {}
