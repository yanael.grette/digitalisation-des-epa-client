import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { RouterModule } from '@angular/router';


import { MaterialModule } from "../angular-material/angular-material.module";

import { NavMenuComponent } from "./nav-menu.component";
import { NavMenuAssistanteComponent } from "./nav-menu-assistante/nav-menu-assistante.component";
import { NavMenuCollaborateurComponent } from "./nav-menu-collaborateur/nav-menu-collaborateur.component";
import { NavMenuCommercialComponent } from "./nav-menu-commercial/nav-menu-commercial.component";
import { NavMenuRHComponent } from "./nav-menu-rh/nav-menu-rh.component";

@NgModule({
  declarations: [ NavMenuComponent, NavMenuAssistanteComponent, NavMenuCollaborateurComponent,
     NavMenuCommercialComponent, NavMenuRHComponent
  ],
  imports: [
    CommonModule,
    MaterialModule,
    RouterModule
  ],
  exports: [NavMenuComponent]
})
export class NavMenuModule {}
