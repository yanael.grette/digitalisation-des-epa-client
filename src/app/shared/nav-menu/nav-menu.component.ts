import { Component } from '@angular/core';
import { KeycloakService } from 'keycloak-angular';

import { Role } from '@shared/utils/roles';

import { environment } from '@env'

/**
  * Le composant du nav menu de base.
  * Ce composant se charge de faire afficher le bon nav pour chaque utilisateur en fonction du rôle de ce dernier.
  * Liste des nav utilisateurs : nav-menu-assistante, nav-menu-collaborateur, nav-menu-commercial et nav-menu-rh.
  * Chaque nav permettra à chaque utilisateur d'avoir sa navigation.
  * Le nav menu pour tous affichera affiche les informs de l'utilisateur (nom+prénom) et permettra de se déconnecter.
  */
@Component({
  selector: 'app-nav-menu',
  templateUrl: './nav-menu.component.html',
})
export class NavMenuComponent {

  role = Role;
  /**
    * Le rôle de l'utilisateur.
    */
  userRole : string;
  /**
    * Les informations (nom+prénom) de l'utilisateur.
    */
  userInfo : string;
  constructor(private keycloakService : KeycloakService){
    let clientId = environment.keycloakConfig.clientId;
    this.userRole = this.keycloakService.getKeycloakInstance().resourceAccess[clientId]["roles"][0];
    let profil = keycloakService.getKeycloakInstance().profile;
    this.userInfo = profil.firstName+" "+profil.lastName;
  }

  //isExpanded = false;
  
  async logout() {
    await this.keycloakService.logout();
  }

}
