/**
 * API du serveur de l'application de digitalisation des EP
 * API qui sra utilisée afin de faire communiquer le client et le serveur ainsi que le serveur et la boîte noire.
 *
 * OpenAPI spec version: 1.2.0
 * 
 *
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen.git
 * Do not edit the class manually.
 */
import { EpModel } from './epModel';
import { ThemeModel } from './themeModel';

export interface DemandeFormationModel { 
    id: number;
    status: string;
    libelle: string;
    description: string;
    demandeRH: boolean;
    dateDemande: Date;
    reponse?: boolean;
    commentaireRefus?: string;
    dateDerniereReponse?: Date;
    theme: ThemeModel;
    ep: EpModel;
}