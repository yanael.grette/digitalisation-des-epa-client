/**
 * API du serveur de l'application de digitalisation des EP
 * API qui sra utilisée afin de faire communiquer le client et le serveur ainsi que le serveur et la boîte noire.
 *
 * OpenAPI spec version: 1.2.0
 * 
 *
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen.git
 * Do not edit the class manually.
 *//* tslint:disable:no-unused-variable member-ordering */

import { Inject, Injectable, Optional }                      from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams,
         HttpResponse, HttpEvent }                           from '@angular/common/http';
import { CustomHttpUrlEncodingCodec }                        from '../encoder';

import { Observable }                                        from 'rxjs';

import { EngagementModel } from '../model/engagementModel';
import { ErreurModel } from '../model/erreurModel';

import { BASE_PATH, COLLECTION_FORMATS }                     from '../variables';
import { Configuration }                                     from '../configuration';


@Injectable()
export class EngagementsService {

    protected basePath = 'http://localhost:3000/api';
    public defaultHeaders = new HttpHeaders();
    public configuration = new Configuration();

    constructor(protected httpClient: HttpClient, @Optional()@Inject(BASE_PATH) basePath: string, @Optional() configuration: Configuration) {
        if (basePath) {
            this.basePath = basePath;
        }
        if (configuration) {
            this.configuration = configuration;
            this.basePath = basePath || configuration.basePath || this.basePath;
        }
    }

    /**
     * @param consumes string[] mime-types
     * @return true: consumes contains 'multipart/form-data', false: otherwise
     */
    private canConsumeForm(consumes: string[]): boolean {
        const form = 'multipart/form-data';
        for (const consume of consumes) {
            if (form === consume) {
                return true;
            }
        }
        return false;
    }


    /**
     * 
     * récupérer la liste des engagements
     * @param idBu id de la business unit à laquelle sont rattachées les données à récupérer
     * @param idAgence id de l&#x27;agence à laquelle sont rattachées les données à récupérer
     * @param observe set whether or not to return the data Observable as the body, response or events. defaults to returning the body.
     * @param reportProgress flag to report request and response progress.
     */
    public getEngagements(idBu?: number, idAgence?: number, observe?: 'body', reportProgress?: boolean): Observable<Array<EngagementModel>>;
    public getEngagements(idBu?: number, idAgence?: number, observe?: 'response', reportProgress?: boolean): Observable<HttpResponse<Array<EngagementModel>>>;
    public getEngagements(idBu?: number, idAgence?: number, observe?: 'events', reportProgress?: boolean): Observable<HttpEvent<Array<EngagementModel>>>;
    public getEngagements(idBu?: number, idAgence?: number, observe: any = 'body', reportProgress: boolean = false ): Observable<any> {



        let queryParameters = new HttpParams({encoder: new CustomHttpUrlEncodingCodec()});
        if (idBu !== undefined && idBu !== null) {
            queryParameters = queryParameters.set('idBu', <any>idBu);
        }
        if (idAgence !== undefined && idAgence !== null) {
            queryParameters = queryParameters.set('idAgence', <any>idAgence);
        }

        let headers = this.defaultHeaders;

        // authentication (bearerAuth) required
        if (this.configuration.accessToken) {
            const accessToken = typeof this.configuration.accessToken === 'function'
                ? this.configuration.accessToken()
                : this.configuration.accessToken;
            headers = headers.set('Authorization', 'Bearer ' + accessToken);
        }
        // to determine the Accept header
        let httpHeaderAccepts: string[] = [
            'application/json'
        ];
        const httpHeaderAcceptSelected: string | undefined = this.configuration.selectHeaderAccept(httpHeaderAccepts);
        if (httpHeaderAcceptSelected != undefined) {
            headers = headers.set('Accept', httpHeaderAcceptSelected);
        }

        // to determine the Content-Type header
        const consumes: string[] = [
        ];

        return this.httpClient.request<Array<EngagementModel>>('get',`${this.basePath}/engagements/`,
            {
                params: queryParameters,
                withCredentials: this.configuration.withCredentials,
                headers: headers,
                observe: observe,
                reportProgress: reportProgress
            }
        );
    }

}
