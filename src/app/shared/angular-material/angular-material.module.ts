import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";

import {MatCardModule} from '@angular/material/card';
import { MatButtonModule } from '@angular/material/button';
import {MatInputModule} from '@angular/material/input';
import {MatIconModule} from '@angular/material/icon';
import {MatMenuModule} from '@angular/material/menu';
import {MatPaginatorModule} from '@angular/material/paginator';
import {MatTableModule} from '@angular/material/table';
import {MatSortModule} from '@angular/material/sort';
import {MatProgressSpinnerModule} from '@angular/material/progress-spinner';
import {MatTabsModule} from '@angular/material/tabs';
import {MatFormFieldModule} from '@angular/material/form-field';
import {MatDatepickerModule} from '@angular/material/datepicker';
import {MatNativeDateModule} from '@angular/material/core';
import {MatCheckboxModule} from '@angular/material/checkbox';
import {MatSelectModule} from '@angular/material/select';


import { NgxMatDatetimePickerModule, NgxMatTimepickerModule, NgxMatNativeDateModule } from '@angular-material-components/datetime-picker';


/**
  * Module qui va faire l'import et l'export de tous les componsants material design qui seront utilisés dans l'application.
  */
@NgModule({
  imports : [MatCardModule,
    MatButtonModule, MatMenuModule,
    MatIconModule, MatPaginatorModule,
    MatSortModule, MatTableModule,
    MatInputModule, MatProgressSpinnerModule,
    MatTabsModule, MatFormFieldModule,
    NgxMatDatetimePickerModule, MatDatepickerModule,
    NgxMatNativeDateModule, MatNativeDateModule,
    MatCheckboxModule, MatSelectModule
  ],
  exports : [MatCardModule,
    MatButtonModule, MatMenuModule,
    MatIconModule, MatPaginatorModule,
    MatSortModule, MatTableModule,
    MatInputModule, MatProgressSpinnerModule,
    MatTabsModule, MatFormFieldModule,
    NgxMatDatetimePickerModule, MatDatepickerModule,
    NgxMatNativeDateModule, MatNativeDateModule,
    MatCheckboxModule, MatSelectModule
  ]
})
export class MaterialModule {}
